﻿using System;
using System.IO;
using System.Linq;

class AntiQuickSort
{
    private static void Swap<Template>(ref Template firstElement, ref Template secondElement)
    {
        Template swapHelper = firstElement;
        firstElement = secondElement;
        secondElement = swapHelper;
    }

    public static void Main()
    {
        int n;
        using (var inputFile = new StreamReader("input.txt"))
        {
            n = int.Parse(inputFile.ReadLine());
        }
        int[] antiArray = Enumerable.Range(1, n).ToArray();

        for (int i = 2; i < n; i++)
        {
            Swap(ref antiArray[i], ref antiArray[i / 2]);
        }
        using (var outputFile = new StreamWriter("output.txt"))
        {
            outputFile.WriteLine(String.Join(" ", antiArray));
        }
    }
}
