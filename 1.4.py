file = open('input.txt').read().split('\n')
data = sorted(list(enumerate(map(float, file[1].split(' ')))), key = lambda t: t[1])
open('output.txt', 'w').write(str(data[0][0] + 1) + ' ' + str(data[int(file[0]) // 2][0] + 1) + ' ' + str(data[-1][0] + 1))